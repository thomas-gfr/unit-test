<?php
use PHPUnit\Framework\TestCase;
use App\User;

class UserTest extends TestCase
{
    public function testTellName()
    {
        $user = new User(25, "John Doe");
        $this->assertEquals("My name is John Doe.", $user->tellName());
    }

    public function testTellAge()
    {
        $user = new User(25, "John Doe");
        $this->assertEquals("I am 25 years old.", $user->tellAge());
    }

    public function testAddFavoriteMovie()
    {
        $user = new User(25, "John Doe");
        $user->addFavoriteMovie("Inception");
        $this->assertContains("Inception", $user->favorite_movies);
    }

    public function testRemoveFavoriteMovie()
    {
        $user = new User(25, "John Doe");
        $user->addFavoriteMovie("Inception");
        $user->removeFavoriteMovie("Inception");
        $this->assertNotContains("Inception", $user->favorite_movies);
    }

    public function testRemoveUnknownFavoriteMovie()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Unknown movie: Avatar");

        $user = new User(25, "John Doe");
        $user->removeFavoriteMovie("Avatar");
    }
}

